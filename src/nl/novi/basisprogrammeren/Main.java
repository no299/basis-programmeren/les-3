package nl.novi.basisprogrammeren;

import nl.novi.basisprogrammeren.persons.Address;
import nl.novi.basisprogrammeren.persons.NonHomelessPerson;
import nl.novi.basisprogrammeren.shapes.Circle;
import nl.novi.basisprogrammeren.shapes.Rectangle;
import nl.novi.basisprogrammeren.shapes.Shape;

public class Main {

    public static void main(String[] args) {
        NonHomelessPerson person = new NonHomelessPerson("Mathijs");
        person.setLastName("Blok");

        Address address = new Address();
        address.setStreet("Hoofdweg");
        person.setAddress(address);

        System.out.println(person.getFullName());
        System.out.println(person.getStreet());

        Shape shape = new Rectangle();
        System.out.println(shape.getAngles());
    }
}
