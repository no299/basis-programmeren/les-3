package nl.novi.basisprogrammeren.shapes;

public class Rectangle implements Shape{

    @Override
    public int getAngles() {
        return 4;
    }
}
