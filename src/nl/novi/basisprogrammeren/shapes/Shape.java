package nl.novi.basisprogrammeren.shapes;

public interface Shape {

    int getAngles();
}
