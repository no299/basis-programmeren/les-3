package nl.novi.basisprogrammeren.persons;

public class NonHomelessPerson extends Person {

    private Address address;

    public NonHomelessPerson(String firstName) {
        super(firstName);
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getStreet() {
        if(this.address != null){
            return this.address.getStreet();
        }else {
            return "";
        }
    }
}
