package nl.novi.basisprogrammeren.persons;

public class Person {

    private String firstName;
    private String lastName;

    public Person(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        if (this.lastName == null) {
            return this.firstName;
        } else {
            return this.firstName + " " + this.lastName;
        }
    }
}
